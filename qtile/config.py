# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import re 
import socket
import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal


mod = "mod4"
# terminal = guess_terminal()
terminal = "alacritty"

keys = [
    # Switch between windows in current stack pane
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "Down", lazy.layout.down(),
        desc="Move focus down in stack pane"),
    Key([mod], "Up", lazy.layout.up(),
        desc="Move focus up in stack pane"),

    # Move windows up or down in current stack
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(),
        desc="Move window down in current stack "),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(),
        desc="Move window up in current stack "),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),

    # Increase/Decrease size of Master
    Key([mod, "control"], "Left", lazy.layout.shrink(), desc="Increase size of master"),
    Key([mod, "control"], "Right", lazy.layout.grow(), desc="Decrease size of master"),

    # Treetab controlls
    Key([mod, "control"], "Up", lazy.layout.section_up(), desc='Move up a section in treetab'),
    Key([mod, "control"], "Down", lazy.layout.section_down(), desc='Move down a section in treetab'),

    # Min/Maximize && Normalize
    # Key([mod], "n", lazy.layout.normalize(), desc='normalize window size ratios'),
    # Key([mod], "m", lazy.layout.maximize(), desc='toggle window between minimum and maximum sizes'),
    
    # Maximize and Float
    Key([mod, "shift"], "m", lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
    Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc='toggle floating'),

    # Switch window focus to other pane(s) of stack
    Key([mod], "Tab", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate(),
        desc="Swap panes of split stack"),

    #Nitrogen Restore
    # Key([mod] "n", lazy.spawn("nitrogen --restore")),
    # Key([mod, "shift"], "n", lazy.spawncmd("nitrogen --restore")),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    # Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
    #     desc="Toggle between split and unsplit sides of stack"),
    
    #Launch Terminal
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),

    # Close Window
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),
    
    # System Kill/Restart
    Key([mod, "shift"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown qtile"),

    # Run Prompts
    Key([mod], "p", lazy.spawn("rofi -show run"), desc="Run Rofi"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
]

# groups = [Group(i) for i in "123456789"]
group_names = [
    ("WWW", {"layout": "treetab"}),
    ("DEV", {"layout": "monadtall"}),
    ("SYS", {"layout": "monadtall"}),
    ("ZOM", {"layout": "monadtall"}),
    ("BOX", {"layout": "monadtall"}),
    ("MUS", {"layout": "monadtall"}),
    ("STM", {"layout": "monadtall"}),
    ("SLK", {"layout": "monadtall"}),
    ("DIS", {"layout": "monadtall"})
]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))


# for i in groups:
#     keys.extend([
#         # mod1 + letter of group = switch to group
#         Key([mod], i.name, lazy.group[i.name].toscreen(),
#             desc="Switch to group {}".format(i.name)),

#         # mod1 + shift + letter of group = switch to & move focused window to group
#         Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
#             desc="Switch to & move focused window to group {}".format(i.name)),
#         # Or, use below if you prefer not to switch to that group.
#         # # mod1 + shift + letter of group = move focused window to group
#         # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
#         #     desc="move focused window to group {}".format(i.name)),
#     ])

##  LAYOUTS

layout_theme = {
    "border_width": 2,
    "margin": 6,
    "border_focus": "AD69AF",
    "border_normal": "1D2330"
}

layouts = [
    #layout.Stack(num_stacks=2),
    #layout.Stack(),
    #layout.Bsp(),
    #layout.Columns(), #Good
    #layout.Matrix(), #Fine
    #layout.MonadWide(),
    #layout.RatioTile(**layout_theme),
    #layout.Tile(**layout_theme), #might be nice
    #layout.VerticalTile(),
    #layout.Max(max_border_width=2),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme), 
    layout.TreeTab(
         font = "Ubuntu",
         fontsize = 10,
         sections = ["WATCH", "WORK", "WRITE"],
         section_fontsize = 12,
         bg_color = "141414",
         active_bg = "90C435",
         active_fg = "000000",
         inactive_bg = "384323",
         inactive_fg = "a0a0a0",
         padding_y = 5,
         padding_x = 5,
         border_width = 2,
         section_top = 10,
         panel_width = 50 #120 originally 320

    )
]

# ##Colors
# colors = [["#292d3e", "#292d3e"], # panel background
#           ["#434758", "#434758"], # background for current screen tab
#           ["#ffffff", "#ffffff"], # font color for group names
#           ["#ff5555", "#ff5555"], # border line color for current tab
#           ["#8d62a9", "#8d62a9"], # border line color for other tab and odd widgets
#           ["#668bd7", "#668bd7"], # color for the even widgets
#           ["#e1acff", "#e1acff"]] # window name

##DEFAULT WIDGETS
widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=3
)
extension_defaults = widget_defaults.copy()

screens = [
    # Screen 1
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(),
                widget.Prompt(),
                widget.TextBox("|"),
                widget.WindowName(),
                widget.TextBox("|"),
                widget.CurrentLayout(),
                widget.TextBox("|"),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                # widget.TextBox("default config", name="default"),
                # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Systray(),
                widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
                widget.QuickExit(),
            ],
            24,
        ),
    ),
    # Screen 2
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(),
                widget.Prompt(),
                widget.TextBox("|"),
                widget.WindowName(),
                widget.TextBox("|"),
                widget.CurrentLayout(),
                widget.TextBox("|"),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                # widget.TextBox("default config", name="default"),
                # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Systray(),
                widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
                widget.QuickExit(),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
# floating_layout = layout.Floating(float_rules=[
#     # Run the utility of `xprop` to see the wm class and name of an X client.
#     {'wmclass': 'confirm'},
#     {'wmclass': 'dialog'},
#     {'wmclass': 'download'},
#     {'wmclass': 'error'},
#     {'wmclass': 'file_progress'},
#     {'wmclass': 'notification'},
#     {'wmclass': 'splash'},
#     {'wmclass': 'toolbar'},
#     {'wmclass': 'confirmreset'},  # gitk
#     {'wmclass': 'makebranch'},  # gitk
#     {'wmclass': 'maketag'},  # gitk
#     {'wname': 'branchdialog'},  # gitk
#     {'wname': 'pinentry'},  # GPG key password entry
#     {'wmclass': 'ssh-askpass'},  # ssh-askpass
# ])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

# @hook.subscribe.startup_once
# def start_once():
#     home = os.path.expanduser('~')
#     subprocess.call([home + '/.config/qtile/autostart.sh'])

# startUp = 0
# while startUp < 1:
#     lazy.spawn("nitrogen --restore")
#     startUp+=1

# @hook.subscribe.startup_once
# def autostart():
#     lazy.spawn("nitrogen --restore")
    # lazy.spawn("picom")
    # home = os.path.expanduser('~')
    # subprocess.Popen([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

